### Deploy

```bash
cd ops
docker-compose up -d
```

The API endpoint is available at `http://localhost:93/`.

Each call requires a User access key, sent in `X-AUTH-TOKEN` request header.

The user API Key:
```
# docker-compose logs php | grep username
! [NOTE] Use the [05dc84dbfb54d66b] api key for [alexbusu] demo username.
```

### API Endpoints

- `GET /tasks` - list the user uncompleted tasks list
- `POST /tasks` - create a new task for user (`body` parameter is required, urlencoded form)
- `PATCH /tasks/{{task_id}}/complete` - mark a task as completed

No OpenAPI/Swagger specs added... See `tests/functional/tests.http` for more info.

### Running tests

- PHPUnit
```bash
docker-compose run php vendor/bin/phpunit
```
- Functional (PHPStorm/IDEA): `tests/functional/tests.http` file (Run all requests in the file > with no environment)

### Domain Events

The Domain Events are sotred into MySQL DB (`event_store` table), and logged into `./var/log/events.log` log file, e.g.
```
2021-01-18T12:15:01+00:00 - {"userId":"31b33456-30b9-4f08-9566-c4eab21f43ea","taskId":"770c8e9f-6197-4f54-91c0-ebb714b0f60d","occurredOn":"2021-01-18T12:15:01+00:00"}
2021-01-18T12:15:01+00:00 - {"userId":"31b33456-30b9-4f08-9566-c4eab21f43ea","taskId":"770c8e9f-6197-4f54-91c0-ebb714b0f60d","occurredOn":"2021-01-18T12:15:01+00:00","taskStartedOn":"2021-01-18T12:15:01+00:00","taskCompletedOn":"2021-01-18T12:15:01+00:00"}
```
Some Event Subscribers might send info to app logger service, configured to persist them in `./var/log/dev.log` log file (look for `app.INFO` keyword), e.g.:
```
app.INFO: the email was sent for task 770c8e9f-6197-4f54-91c0-ebb714b0f60d {"event":{"App\\EventHandlers\\DomainEvents\\TaskWasCreated":[]}}
app.INFO: the task [770c8e9f-6197-4f54-91c0-ebb714b0f60d] was completed in 1 second(s) {"event":{"App\\EventHandlers\\DomainEvents\\TaskWasCompleted":[]}}
```

### Project Directory Structure

The project structure is of a typical Symfony 4/5 app.

Added directories:

- `./ops` - build and deployment configuration (docker, docker-compose)

---

<p style="text-align: center;">busuioc.alexandru@gmail.com</p>
