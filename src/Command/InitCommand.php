<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

class InitCommand extends Command
{
    protected static $defaultName = 'app:init';
    private UserRepository $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoder;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        parent::__construct(null);
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    protected function configure()
    {
        $this->setDescription('Will setup the database with initial data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // run migrations
        $migrationCommand = $this->getApplication()->get('doctrine:migrations:migrate');
        $migrationsInput = new ArrayInput([]);
        $migrationsInput->setInteractive(false);
        $migrationCommand->run($migrationsInput, $output);

        $username = 'alexbusu';
        $apiKey = '05dc84dbfb54d66b'; // e.g. bin2hex(random_bytes(8));
        if (!$this->userRepository->findOneBy(['username' => $username])) {
            $user = new User(Uuid::v4()); // a factory would be better to use
            $user->setUsername($username);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($apiKey);
            $this->userRepository->add($user);
            $io->success('A new user was added.');
        } else {
            $io->success('The user exists.');
        }
        $io->note("Use the [{$apiKey}] api key for [{$username}] demo username.");

        return Command::SUCCESS;
    }
}
