<?php

/** @noinspection PhpDocSignatureInspection */

namespace App\Controller;

use App\Entity\User;
use App\Repository\TaskRepository;
use App\Services\CreateNewTask\CreateNewTaskRequest;
use App\Services\CreateNewTask\CreateNewTaskService;
use App\Services\MarkUserTaskCompleted\MarkUserTaskCompletedRequest;
use App\Services\MarkUserTaskCompleted\MarkUserTaskCompletedService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

class TasksController extends AbstractController
{
    const ITEMS_PER_PAGE = 20;

    /**
     * @Route("/tasks", name="list_tasks", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @return Response
     */
    public function listTasks(
        TokenStorageInterface $tokenStorage,
        TaskRepository $taskRepository,
        Request $request,
        SerializerInterface $serializer
    ): Response {
        $page = ($_ = (int)$request->query->get('page', 1)) > 0 ? $_ : 1;
        $perPage = self::ITEMS_PER_PAGE;

        $tasks = $taskRepository->findUncompletedTasks($tokenStorage->getToken()->getUser(), [
            'createdOn' => 'DESC',
        ], $perPage, ($page - 1) * $perPage);

        return $this->json([
            'data' => $serializer->normalize($tasks, 'json', ['groups' => 'task:list:own']),
        ]);
    }

    /**
     * @Route("/tasks", name="create_task", methods={"POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function createTask(
        TokenStorageInterface $tokenStorage,
        Request $request,
        CreateNewTaskService $createNewTask,
        SerializerInterface $serializer
    ): Response {
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        $uid = $user->getId();
        $taskBody = (string)$request->request->get('body');
        $createRequest = new CreateNewTaskRequest($uid, $taskBody);
        $response = $createNewTask->execute($createRequest);
        return $this->json([
            'data' => $serializer->normalize($response->getTask(), 'json', ['groups' => 'task:get']),
        ], Response::HTTP_CREATED);
    }

    /**
     * @Route("/tasks/{id}/complete", name="complete_task", methods={"PATCH"}, requirements={"id": "([a-z0-9\-]{36})"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function markCompleted(
        string $id,
        TokenStorageInterface $tokenStorage,
        MarkUserTaskCompletedService $markUserTaskCompleted,
        SerializerInterface $serializer
    ): Response {
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        $markTaskCompleteRequest = new MarkUserTaskCompletedRequest($user->getId(), $id);
        $response = $markUserTaskCompleted->execute($markTaskCompleteRequest);
        return $this->json([
            'data' => $serializer->normalize($response->getTask(), 'json', ['groups' => 'task:get']),
        ]);
    }
}
