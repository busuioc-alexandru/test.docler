<?php

namespace App\Entity;

use App\Repository\EventStoreRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventStoreRepository::class)
 * @ORM\Table(name="events_store")
 */
class StoredEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $typeName;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeImmutable $occurredOn;

    /**
     * @ORM\Column(type="text")
     */
    private string $eventBody;

    public function __construct(
        string $typeName,
        DateTimeImmutable $occurredOn,
        string $eventBody
    ) {
        $this->typeName = $typeName;
        $this->occurredOn = $occurredOn;
        $this->eventBody = $eventBody;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function typeName(): string
    {
        return $this->typeName;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function eventBody(): string
    {
        return $this->eventBody;
    }
}
