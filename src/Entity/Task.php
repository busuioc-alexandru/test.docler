<?php

namespace App\Entity;

use App\EventHandlers\DomainEventPublisher;
use App\EventHandlers\DomainEvents\TaskWasCompleted;
use App\EventHandlers\DomainEvents\TaskWasCreated;
use App\Repository\TaskRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true)
     * @Groups({"task:get", "task:list:own"})
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", name="uid_id")
     */
    private User $user;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"task:get", "task:list:own"})
     */
    private DateTimeInterface $createdOn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"task:get", "task:list:own"})
     */
    private ?DateTimeInterface $completedOn = null;

    /**
     * @ORM\Column(type="text")
     * @Groups({"task:get", "task:list:own"})
     */
    private string $body;

    public function __construct(User $user, string $body)
    {
        $this->id = Uuid::v4()->toRfc4122();
        $this->createdOn = new DateTimeImmutable();
        $this->user = $user;
        $this->body = $body;

        $this->publishCreatedNewTaskEvent();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedOn(): DateTimeInterface
    {
        return $this->createdOn;
    }

    public function getCompletedOn(): ?DateTimeInterface
    {
        return $this->completedOn;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function belongsTo(User $user)
    {
        return $this->user->getId() == $user->getId();
    }

    public function markCompleted(): self
    {
        $this->completedOn = new DateTimeImmutable();

        $this->publishTaskWasCompletedEvent();
        return $this;
    }

    private function publishCreatedNewTaskEvent(): void
    {
        DomainEventPublisher::instance()->publish(
            new TaskWasCreated(
                Uuid::fromString($this->user->getId()),
                Uuid::fromString($this->id)
            )
        );
    }

    private function publishTaskWasCompletedEvent(): void
    {
        $event = new TaskWasCompleted(
            Uuid::fromString($this->user->getId()),
            Uuid::fromString($this->id),
            DateTimeImmutable::createFromFormat('U', $this->createdOn->getTimestamp()),
            DateTimeImmutable::createFromFormat('U', $this->completedOn->getTimestamp())
        );
        DomainEventPublisher::instance()->publish($event);
    }
}
