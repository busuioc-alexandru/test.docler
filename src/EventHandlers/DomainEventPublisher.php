<?php

namespace App\EventHandlers;

use App\EventHandlers\DomainEvents\DomainEvent;
use App\EventHandlers\Subscriber\DomainEventSubscriber;
use BadMethodCallException;

class DomainEventPublisher
{
    private array $subscribers = [];
    private static DomainEventPublisher $instance;

    public static function instance(): self
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function __clone()
    {
        throw new BadMethodCallException(
            'clone is not supported'
        );
    }

    public function subscribe(DomainEventSubscriber $domainEventSubscriber): void
    {
        $this->subscribers[] = $domainEventSubscriber;
    }

    public function publish(DomainEvent $theEvent): void
    {
        foreach ($this->subscribers as $aSubscriber) {
            if ($aSubscriber->isSubscribedTo($theEvent)) {
                $aSubscriber->handle($theEvent);
            }
        }
    }
}
