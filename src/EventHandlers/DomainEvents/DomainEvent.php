<?php

namespace App\EventHandlers\DomainEvents;

use DateTimeImmutable;

interface DomainEvent
{
    public function getOccurredOn(): DateTimeImmutable;
}
