<?php

namespace App\EventHandlers\DomainEvents;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class TaskWasCompleted implements DomainEvent
{
    private DateTimeImmutable $occurredOn;
    private Uuid $userId;
    private Uuid $taskId;
    private DateTimeImmutable $taskStartedOn;
    private DateTimeImmutable $taskCompletedOn;

    public function __construct(
        Uuid $userId,
        Uuid $taskId,
        DateTimeImmutable $taskStartedOn,
        DateTimeImmutable $taskCompletedOn
    ) {
        $this->occurredOn = new DateTimeImmutable();
        $this->userId = $userId;
        $this->taskId = $taskId;
        $this->taskStartedOn = $taskStartedOn;
        $this->taskCompletedOn = $taskCompletedOn;
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getOccurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function getTaskStartedOn(): DateTimeImmutable
    {
        return $this->taskStartedOn;
    }

    public function getTaskCompletedOn(): DateTimeImmutable
    {
        return $this->taskCompletedOn;
    }
}
