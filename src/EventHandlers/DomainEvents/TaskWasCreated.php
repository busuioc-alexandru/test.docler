<?php

namespace App\EventHandlers\DomainEvents;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class TaskWasCreated implements DomainEvent
{
    private DateTimeImmutable $occurredOn;
    private Uuid $userId;
    private Uuid $taskId;

    public function __construct(Uuid $userId, Uuid $taskId)
    {
        $this->userId = $userId;
        $this->taskId = $taskId;
        $this->occurredOn = new DateTimeImmutable();
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getTaskId(): Uuid
    {
        return $this->taskId;
    }

    public function getOccurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
