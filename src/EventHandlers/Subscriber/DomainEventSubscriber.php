<?php

namespace App\EventHandlers\Subscriber;

use App\EventHandlers\DomainEvents\DomainEvent;

interface DomainEventSubscriber
{
    public function handle(DomainEvent $event): void;

    public function isSubscribedTo(DomainEvent $event): bool;
}
