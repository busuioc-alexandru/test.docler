<?php

namespace App\EventHandlers\Subscriber;

use App\EventHandlers\DomainEvents\DomainEvent;
use App\EventHandlers\DomainEvents\TaskWasCreated;
use Psr\Log\LoggerInterface;

class EmailTaskWasCreatedSubscriber implements DomainEventSubscriber
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle(DomainEvent $event): void
    {
        // TODO: send the email
        // just for POC notifications, will log the intent
        /** @var TaskWasCreated $event */
        $this->logger->info("the email was sent for task " . $event->getTaskId(), ['event' => $event]);
    }

    public function isSubscribedTo(DomainEvent $event): bool
    {
        return get_class($event) == TaskWasCreated::class;
    }
}
