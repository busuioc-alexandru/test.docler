<?php

namespace App\EventHandlers\Subscriber;

use App\EventHandlers\DomainEvents\DomainEvent;
use Symfony\Component\Serializer\SerializerInterface;

class LogDomainEventSubscriber implements DomainEventSubscriber
{
    private SerializerInterface $serializer;
    private $fh;

    public function __construct(string $logFilePath, SerializerInterface $serializer)
    {
        $this->fh = fopen($logFilePath, 'a');
        $this->serializer = $serializer;
    }

    public function handle(DomainEvent $event): void
    {
        fwrite($this->fh, $this->formatLogEntry($event) . PHP_EOL);
    }

    public function isSubscribedTo(DomainEvent $event): bool
    {
        return true;
    }

    private function formatLogEntry(DomainEvent $event): string
    {
        return sprintf(
            '%s - %s',
            $event->getOccurredOn()->format(DATE_ATOM),
            $this->serializer->serialize($event, 'json')
        );
    }
}
