<?php

namespace App\EventHandlers\Subscriber;

use App\EventHandlers\DomainEvents\DomainEvent;
use App\EventHandlers\DomainEvents\TaskWasCompleted;
use Psr\Log\LoggerInterface;

class LogTaskDurationSubscriber implements DomainEventSubscriber
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function handle(DomainEvent $event): void
    {
        // TODO: log the duration to OLAP system
        // just for POC notifications, will log the intent
        /** @var TaskWasCompleted $event */
        $this->logger->info($this->formatTheLogEntry($event), ['event' => $event]);
    }

    public function isSubscribedTo(DomainEvent $event): bool
    {
        return get_class($event) == TaskWasCompleted::class;
    }

    private function formatTheLogEntry(TaskWasCompleted $event): string
    {
        return sprintf('the task [%s] was completed in %s', $event->getTaskId(), $this->getFriendlyTimeDiff($event));
    }

    private function getFriendlyTimeDiff(TaskWasCompleted $event): string
    {
        $diff = $event->getTaskStartedOn()->diff($event->getTaskCompletedOn());
        switch (true) {
            case $diff->days > 0:
                return "{$diff->days} day(s)";
            case $diff->h > 0:
                return "{$diff->h} hour(s)";
            case $diff->i > 0:
                return "{$diff->i} minute(s)";
            case $diff->s > 0:
                return "{$diff->s} second(s)";
        }
        return 'N/A';
    }
}