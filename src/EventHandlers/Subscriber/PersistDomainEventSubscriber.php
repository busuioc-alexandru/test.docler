<?php

namespace App\EventHandlers\Subscriber;

use App\EventHandlers\DomainEvents\DomainEvent;
use App\Repository\EventStoreRepositoryInterface;

class PersistDomainEventSubscriber implements DomainEventSubscriber
{
    private EventStoreRepositoryInterface $eventStore;

    public function __construct(EventStoreRepositoryInterface $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function handle(DomainEvent $event): void
    {
        $this->eventStore->append($event);
    }

    public function isSubscribedTo(DomainEvent $event): bool
    {
        return true;
    }
}
