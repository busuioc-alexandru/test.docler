<?php

namespace App\EventSubscriber;

use App\Controller\TasksController;
use App\EventHandlers\DomainEventPublisher;
use App\EventHandlers\Subscriber\EmailTaskWasCreatedSubscriber;
use App\EventHandlers\Subscriber\LogDomainEventSubscriber;
use App\EventHandlers\Subscriber\LogTaskDurationSubscriber;
use App\EventHandlers\Subscriber\PersistDomainEventSubscriber;
use App\Exception\UserFacingException;
use App\Repository\EventStoreRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\SerializerInterface;
use function get_class;

class OnRequestSubscriber implements EventSubscriberInterface
{
    private EventStoreRepository $eventStoreRepository;
    private LoggerInterface $logger;
    private SerializerInterface $serializer;
    private ParameterBagInterface $parameterBag;

    public function __construct(
        EventStoreRepository $eventStoreRepository,
        LoggerInterface $logger,
        SerializerInterface $serializer,
        ParameterBagInterface $parameterBag
    ) {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::REQUEST => [
                ['initializeDomainEventPublisher', 10],
            ],
            KernelEvents::CONTROLLER => [
                ['onKernelController', 0],
            ],
            KernelEvents::EXCEPTION => [
                ['onKernelException', 10],
            ],
        ];
    }

    /** @noinspection PhpUnused */
    public function initializeDomainEventPublisher(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $logsDir = $this->parameterBag->get('kernel.logs_dir');
        $domainEventPublisher = DomainEventPublisher::instance();
        $domainEventPublisher->subscribe(new PersistDomainEventSubscriber($this->eventStoreRepository));
        $domainEventPublisher->subscribe(new LogDomainEventSubscriber($logsDir . '/events.log', $this->serializer));
        // A better approach would be sending the events to a messaging queue,
        // like RabbitMQ, and to process them with a worker (might be a CLI
        // Command implemented in the curent application).
        // Processing the Domain Events at runtime would be necessary, as I see,
        // when a component has to be notified about some changes.
    }

    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();

        if (is_array($controller)) {
            $controller = $controller[0];
        }

        switch (get_class($controller)) {
            // We might use this approach to send events to specific RabbitMQ (mentioned above) queue.
            case TasksController::class:
                $domainEventPublisher = DomainEventPublisher::instance();
                $domainEventPublisher->subscribe(new EmailTaskWasCreatedSubscriber($this->logger));
                $domainEventPublisher->subscribe(new LogTaskDurationSubscriber($this->logger));
                break;
        }
    }

    /** @noinspection PhpUnused */
    public function onKernelException(ExceptionEvent $event)
    {
        $event->allowCustomResponseCode();

        $throwable = $event->getThrowable();
        $responseStatus = [
                AccessDeniedException::class => Response::HTTP_FORBIDDEN,
            ][get_class($throwable)] ?? Response::HTTP_INTERNAL_SERVER_ERROR;

        $responseMessage = [
                AccessDeniedException::class => 'authentication is required',
            ][get_class($throwable)] ?? ($throwable instanceof UserFacingException
                ? $throwable->getMessage()
                : 'internal error');

        $responseData = [
            'message' => $responseMessage,
        ];

        if ($this->parameterBag->get('kernel.environment') === 'dev') {
            $responseData['message:dev'] = $throwable->getMessage();
        }

        $event->setResponse(new JsonResponse($responseData, $responseStatus));
    }
}
