<?php

namespace App\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements UserFacingException
{
}
