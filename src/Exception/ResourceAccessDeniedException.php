<?php

namespace App\Exception;

use Exception;

class ResourceAccessDeniedException extends Exception implements UserFacingException
{
}
