<?php

namespace App\Repository;

use App\Entity\StoredEvent;
use App\EventHandlers\DomainEvents\DomainEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @method StoredEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method StoredEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method StoredEvent[]    findAll()
 * @method StoredEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventStoreRepository extends ServiceEntityRepository implements EventStoreRepositoryInterface
{
    private SerializerInterface $serializer;

    public function __construct(ManagerRegistry $registry, SerializerInterface $serializer)
    {
        parent::__construct($registry, StoredEvent::class);
        $this->serializer = $serializer;
    }

    public function append(DomainEvent $aDomainEvent): void
    {
        $storedEvent = new StoredEvent(
            get_class($aDomainEvent),
            $aDomainEvent->getOccurredOn(),
            $this->serializer->serialize($aDomainEvent, 'json')
        );

        $this->getEntityManager()->persist($storedEvent);
    }

    public function allStoredEventsSince(int $anEventId): array
    {
        $query = $this->createQueryBuilder('e');
        if ($anEventId) {
            $query->where('e.eventId > :eventId');
            $query->setParameters(['eventId' => $anEventId]);
        }
        $query->orderBy('e.eventId');

        return $query->getQuery()->getResult();
    }
}
