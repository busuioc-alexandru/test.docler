<?php

namespace App\Repository;

use App\EventHandlers\DomainEvents\DomainEvent;

interface EventStoreRepositoryInterface
{
    public function append(DomainEvent $aDomainEvent): void;

    public function allStoredEventsSince(int $anEventId): array;
}
