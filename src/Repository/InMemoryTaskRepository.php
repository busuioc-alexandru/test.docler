<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Common\Collections\ArrayCollection;

class InMemoryTaskRepository implements TaskRepositoryInterface
{
    /**
     * @var ArrayCollection|Task[]
     * @psalm-var ArrayCollection<int, Task>
     */
    private ArrayCollection $tasks;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function add(Task $task): void
    {
        $this->tasks->set($task->getId(), $task);
    }

    public function byId(string $id): ?Task
    {
        return $this->tasks->get($id);
    }
}