<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class InMemoryUserRepository implements UserRepositoryInterface
{
    /**
     * @var ArrayCollection|User[]
     * @psalm-var ArrayCollection<int, User>
     */
    private ArrayCollection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function add(User $user): void
    {
        $this->users->set($user->getId(), $user);
    }

    public function byId(string $id): ?User
    {
        return $this->users->get($id);
    }
}
