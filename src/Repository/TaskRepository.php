<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository implements TaskRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function add(Task $task): void
    {
        $this->_em->persist($task);
        $this->_em->flush();
    }

    public function byId(string $id): ?Task
    {
        return $this->find($id);
    }

    public function findUncompletedTasks(
        UserInterface $user,
        array $orderBy,
        int $limit,
        int $offset = null
    ) {
        $taskTableAlias = 't';
        $qb = $this->createQueryBuilder($taskTableAlias);
        $qb
            ->where('t.user = :user')
            ->andWhere($qb->expr()->isNull('t.completedOn'))
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        foreach ($orderBy as $column => $order) {
            $qb->addOrderBy("{$taskTableAlias}.{$column}", $order);
        }
        return $qb->getQuery()->execute(['user' => $user]);
    }
}
