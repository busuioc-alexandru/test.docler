<?php

namespace App\Repository;

use App\Entity\User;

interface UserRepositoryInterface
{
    public function byId(string $id): ?User;

    public function add(User $user): void;
}
