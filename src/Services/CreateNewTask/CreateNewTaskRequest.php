<?php

namespace App\Services\CreateNewTask;

class CreateNewTaskRequest
{
    private string $userId;
    private string $body;

    public function __construct(string $userId, string $body)
    {
        $this->userId = $userId;
        $this->body = $body;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getBody(): string
    {
        return $this->body;
    }
}
