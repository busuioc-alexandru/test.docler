<?php

namespace App\Services\CreateNewTask;

use App\Entity\Task;

class CreateNewTaskResponse
{
    private Task $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function getTask(): Task
    {
        return $this->task;
    }
}
