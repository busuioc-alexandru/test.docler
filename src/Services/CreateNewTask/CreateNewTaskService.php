<?php

namespace App\Services\CreateNewTask;

use App\Exception\InvalidArgumentException;
use App\Exception\ItemNotFoundException;
use App\Repository\TaskRepositoryInterface;
use App\Repository\UserRepositoryInterface;

class CreateNewTaskService
{
    private UserRepositoryInterface $userRepository;
    private TaskRepositoryInterface $taskRepository;

    public function __construct(
        TaskRepositoryInterface $taskRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->taskRepository = $taskRepository;
    }

    public function execute(CreateNewTaskRequest $request)
    {
        $taskBody = $request->getBody();
        if (!$taskBody) {
            throw new InvalidArgumentException("task content is expected");
        }

        $uid = $request->getUserId();
        if (!($user = $this->userRepository->byId($uid))) {
            throw new ItemNotFoundException("the user with id [{$uid}] could not be found");
        }

        $task = $user->createNewTask($taskBody);
        $this->taskRepository->add($task);

        return new CreateNewTaskResponse($task);
    }
}
