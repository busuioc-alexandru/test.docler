<?php

namespace App\Services\MarkUserTaskCompleted;

use App\Exception\ItemNotFoundException;
use App\Exception\ResourceAccessDeniedException;
use App\Repository\TaskRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Services\CreateNewTask\CreateNewTaskResponse;
use InvalidArgumentException;

class MarkUserTaskCompletedService
{
    private UserRepositoryInterface $userRepository;
    private TaskRepositoryInterface $taskRepository;

    public function __construct(
        TaskRepositoryInterface $taskRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->taskRepository = $taskRepository;
    }

    public function execute(MarkUserTaskCompletedRequest $request)
    {
        $taskId = $request->getTaskId();
        if (!$taskId) {
            throw new InvalidArgumentException("task id is expected");
        }

        $userid = $request->getUserId();
        if (!$userid) {
            throw new InvalidArgumentException("user id is expected");
        }

        if (!($user = $this->userRepository->byId($userid))) {
            throw new ItemNotFoundException("the user with id [{$userid}] could not be found");
        }

        if (!($task = $this->taskRepository->byId($taskId))) {
            throw new ItemNotFoundException("the task with id [{$taskId}] could not be found");
        }

        if (!$task->belongsTo($user)) {
            throw new ResourceAccessDeniedException("cannot alter this task with current user");
        }

        $task->markCompleted();
        $this->taskRepository->add($task);

        return new CreateNewTaskResponse($task);
    }
}
