<?php

namespace App\Tests\EventHandlers\Subscriber;

use App\EventHandlers\DomainEventPublisher;
use App\EventHandlers\DomainEvents\DomainEvent;
use App\EventHandlers\Subscriber\DomainEventSubscriber;
use BadMethodCallException;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class DomainEventPublisherTest extends TestCase
{
    public function testDomainEventSubscriberIsSubscribedTo()
    {
        $eventSubscriber = new class implements DomainEventSubscriber {
            private bool $isSubscribedWasCalled = false;

            public function handle(DomainEvent $event): void
            {
            }

            public function isSubscribedTo(DomainEvent $event): bool
            {
                $this->isSubscribedWasCalled = true;
                return false;
            }

            public function isSubscribedWasCalled(): bool
            {
                return $this->isSubscribedWasCalled;
            }
        };

        $anEvent = new class implements DomainEvent {
            public function getOccurredOn(): DateTimeImmutable
            {
                return new DateTimeImmutable();
            }
        };

        $domainEventPublisher = new DomainEventPublisher();
        $domainEventPublisher->subscribe($eventSubscriber);
        $domainEventPublisher->publish($anEvent);

        $this->assertTrue($eventSubscriber->isSubscribedWasCalled());
    }

    public function testDomainEventSubscriberHandleWasCalled()
    {
        $eventSubscriber = new class implements DomainEventSubscriber {
            private bool $handleWasCalled = false;

            public function handle(DomainEvent $event): void
            {
                $this->handleWasCalled = true;
            }

            public function isSubscribedTo(DomainEvent $event): bool
            {
                return true;
            }

            public function handleWasCalled(): bool
            {
                return $this->handleWasCalled;
            }
        };

        $anEvent = new class implements DomainEvent {
            public function getOccurredOn(): DateTimeImmutable
            {
                return new DateTimeImmutable();
            }
        };

        $domainEventPublisher = new DomainEventPublisher();
        $domainEventPublisher->subscribe($eventSubscriber);
        $domainEventPublisher->publish($anEvent);

        $this->assertTrue($eventSubscriber->handleWasCalled());
    }

    public function testCloneObjectWillThrow()
    {
        $this->expectException(BadMethodCallException::class);

        $domainEventPublisher = new DomainEventPublisher();

        $cloned = clone $domainEventPublisher;
    }
}
