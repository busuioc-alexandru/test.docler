<?php

namespace App\Tests\Services;

use App\Entity\Task;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Exception\ItemNotFoundException;
use App\Repository\InMemoryTaskRepository;
use App\Repository\InMemoryUserRepository;
use App\Repository\TaskRepositoryInterface;
use App\Services\CreateNewTask\CreateNewTaskRequest;
use App\Services\CreateNewTask\CreateNewTaskResponse;
use App\Services\CreateNewTask\CreateNewTaskService;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class CreateNewTaskServiceTest extends TestCase
{
    protected CreateNewTaskService $service;
    protected InMemoryUserRepository $userRepository;

    protected function setUp(): void
    {
        $this->userRepository = new InMemoryUserRepository();
        $this->service = new CreateNewTaskService(new InMemoryTaskRepository(), $this->userRepository);
    }

    public function testExecuteExpectsExceptionForEmptyBody()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('task content is expected');

        $this->service->execute(new CreateNewTaskRequest('', ''));
    }

    public function testExecuteExpectsExceptionForInexistentUser()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->expectExceptionMessageMatches('/^the user .* could not be found$/i');

        $this->service->execute(new CreateNewTaskRequest('qwerty', 'some task content'));
    }

    public function testExecuteExpectsResponseWithTask()
    {
        $inputUser = $this->addTestUserToRepository();
        $taskInputBody = 'some task content';
        $response = $this->service->execute(new CreateNewTaskRequest($inputUser->getId(), $taskInputBody));

        $this->assertInstanceOf(CreateNewTaskResponse::class, $response);
        $this->assertInstanceOf(Task::class, $task = $response->getTask());
        $this->assertInstanceOf(DateTimeInterface::class, $task->getCreatedOn());
        $this->assertInstanceOf(User::class, $taskUser = $task->getUser());
        $this->assertTrue($inputUser->isSameAs($taskUser));
        $this->assertEquals($taskInputBody, $task->getBody());
    }

    public function testExecuteTriggersAddInTaskRepository()
    {
        $taskRepository = new class implements TaskRepositoryInterface {
            public bool $addWasCalled = false;

            public function byId(string $id): ?Task
            {
                return null;
            }

            public function add(Task $task): void
            {
                $this->addWasCalled = true;
            }

            public function addWasCalled(): bool
            {
                return $this->addWasCalled;
            }
        };

        $service = new CreateNewTaskService($taskRepository, $this->userRepository);

        $inputUser = $this->addTestUserToRepository();

        $service->execute(new CreateNewTaskRequest($inputUser->getId(), 'some task content'));

        $this->assertTrue($taskRepository->addWasCalled());
    }

    private function addTestUserToRepository(): User
    {
        $user = new User(Uuid::v4());
        $this->userRepository->add($user);
        return $user;
    }
}
