<?php

namespace App\Tests\Services;

use App\Entity\Task;
use App\Entity\User;
use App\Exception\ItemNotFoundException;
use App\Exception\ResourceAccessDeniedException;
use App\Repository\InMemoryTaskRepository;
use App\Repository\InMemoryUserRepository;
use App\Services\CreateNewTask\CreateNewTaskResponse;
use App\Services\MarkUserTaskCompleted\MarkUserTaskCompletedRequest as Request;
use App\Services\MarkUserTaskCompleted\MarkUserTaskCompletedService;
use DateTimeInterface;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class MarkUserTaskCompletedServiceTest extends TestCase
{
    protected InMemoryTaskRepository $taskRepository;
    protected InMemoryUserRepository $userRepository;
    protected MarkUserTaskCompletedService $service;

    protected function setUp(): void
    {
        $this->taskRepository = new InMemoryTaskRepository();
        $this->userRepository = new InMemoryUserRepository();
        $this->service = new MarkUserTaskCompletedService($this->taskRepository, $this->userRepository);
    }

    public function testExecuteExpectsExceptionWhenMissingTaskId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('task id is expected');

        $this->service->execute(new Request('', ''));
    }

    public function testExecuteExpectsExceptionWhenMissingUserId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('user id is expected');

        $this->service->execute(new Request('', Uuid::v4()->toRfc4122()));
    }

    public function testExecuteExpectsExceptionWhenUserNotFound()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->expectExceptionMessageMatches('/^the user .* could not be found$/i');

        $this->service->execute(new Request(Uuid::v4()->toRfc4122(), Uuid::v4()->toRfc4122()));
    }

    public function testExecuteExpectsExceptionWhenTaskNotFound()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->expectExceptionMessageMatches('/^the task .* could not be found$/i');

        $userId = Uuid::v4();
        $this->userRepository->add(new User($userId));

        $this->service->execute(new Request($userId->toRfc4122(), Uuid::v4()->toRfc4122()));
    }

    public function testExecuteExpectsExceptionWhenTaskBelongsToOtherUser()
    {
        $this->expectException(ResourceAccessDeniedException::class);
        $this->expectExceptionMessage('cannot alter this task with current user');

        $userWithTask = new User(Uuid::v4());
        $userWithoutTask = new User(Uuid::v4());

        $task = $userWithTask->createNewTask('some content');

        $this->userRepository->add($userWithoutTask);
        $this->userRepository->add($userWithTask);
        $this->taskRepository->add($task);

        $this->service->execute(new Request($userWithoutTask->getId(), $task->getId()));
    }

    public function testExecuteExpectsSuccessfulResponse()
    {
        $userWithTask = new User(Uuid::v4());
        $task = $userWithTask->createNewTask('some content');

        $this->userRepository->add($userWithTask);
        $this->taskRepository->add($task);

        $response = $this->service->execute(new Request($userWithTask->getId(), $task->getId()));

        $this->assertInstanceOf(CreateNewTaskResponse::class, $response);
        $this->assertInstanceOf(Task::class, $responseTask = $response->getTask());
        $this->assertInstanceOf(DateTimeInterface::class, $responseTask->getCompletedOn());
    }
}
